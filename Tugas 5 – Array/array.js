//Soal Nomor 1
function range(angka1,angka2){
    if (!angka1||!angka2) {
        return -1;
    }
    else {   
        let Number=[];
        Number.push(angka1);
        while (angka1<angka2 || angka1>angka2) {
            angka1=angka1<angka2? (angka1+1): (angka1-1)
            Number.push(angka1);
        }
        return Number;
    }
}

console.log(" -------------- Soal Nomor 1 ---------------")  
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//Soal Nomor 2
function rangeWithStep(angka1a,angka2a, step) {
    if (!angka1a || !angka2a) {
        return -1;
    }
    else {
        let Number=[];
        Number.push(angka1a);
        while(angka1a<=(angka2a-step)||angka1a>=(angka2a+step)){
            angka1a=angka1a<angka2a? (angka1a+step): (angka1a-step);
            Number.push(angka1a)
        }
    return Number
    }
}
console.log(" -------------- Soal Nomor 2 ---------------")  
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//Soal Nomor 3

function sum(awalDeret, akhirDeret, step=1) {
    let nilai=0;
    nilai+=awalDeret;
    while (awalDeret<=(akhirDeret-step)||awalDeret>=(akhirDeret+step)) {
        awalDeret=awalDeret<akhirDeret? (awalDeret+step): (awalDeret-step);
        nilai+=awalDeret
    }
    return isNaN(nilai)?0:(nilai);
}

console.log(" -------------- Soal Nomor 3 ---------------")  
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//Soal Nomor 4

function dataHandling(data){
    data.forEach(function bio(nilai) {
        console.log("Nomor ID: " + nilai[0] + "\n" +
                    "Nama Lengkap: " + nilai[1] + "\n" +
                    "TTL: " + nilai[2] + " " + nilai[3] + "\n" +
                    "Hobby: " + nilai[4] +
                    "\n")
    });
}
console.log(" -------------- Soal Nomor 4 ---------------")  
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input)

//Soal Nomor 5

function balikKata(string){
    let curStr = string
    let newStr = ""

    for (let i = string.length - 1; i >= 0; i--) {
        newStr = newStr + curStr[i];
    }
    return newStr
}
console.log(" -------------- Soal Nomor 5 ---------------")  

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal Nomor 6

function dataHandling2(data){

    data.splice(1,1, "Roman Alamsyah Elsharawy")
    data.splice(2, 1, "Provinsi Bandar Lampung")
    data.splice(4, 1, "Pria", "SMA Internasional Metro")

    console.log(data);
    
    

    //Pemisahan Array Tanggal Lahir
    var tanggal = data[3].split("/")
    
        
    
    //Bulan
    var bulan = tanggal[1];
    var namaBulan;
    switch(Number(tanggal[1])) {
        case 1 : { namaBulan = "Januari"; break; }
        case 2 : { namaBulan = "Februari"; break; }
        case 3 : { namaBulan = "Maret"; break; }
        case 4 : { namaBulan = "April"; break; }
        case 5 : { namaBulan = "Mei"; break; }
        case 6 : { namaBulan = "Juni"; break; }
        case 7 : { namaBulan = "Juli"; break; }
        case 8 : { namaBulan = "Agustus"; break; }
        case 9 : { namaBulan = "September"; break; }
        case 10 : { namaBulan = "Oktober"; break; }
        case 11 : { namaBulan = "November"; break; }
        case 12 : { namaBulan = "Desember"; break; }
    }
    console.log(namaBulan);

    //Descending Tanggal
        tanggal.sort(function(a, b){return b-a});
    console.log(tanggal)

    //Tanggal Lahir dd-mm-yyyy
    var forTanggal = data[3].split("/").join("-")
    console.log(forTanggal)

    //Nama
    var nama = data[1].slice(0,14)
    console.log(nama)
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log(" -------------- Soal Nomor 6 ---------------") 

dataHandling2(input);

console.log(" -------------- Selesai ---------------")  
