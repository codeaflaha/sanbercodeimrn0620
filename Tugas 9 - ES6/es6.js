//NOMOR 1
const golden = () => "this is golden!!"
console.log(golden());
console.log('');

//NOMOR 2
const newFunction = (firstName, lastName) => ({firstName: firstName,
    lastName: lastName, fullName : (firstName + " " + lastName) 
      });
console.log(newFunction("William", "Imoh"));
console.log('');

//NOMOR 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation);
console.log('');

//NOMOR 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined =  [...west,...east];
console.log(combined);
console.log('');

//NOMOR 5
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before);
console.log('');
