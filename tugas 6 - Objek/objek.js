// Soal 1
function arrayToObject(arr) {
    var d = {};
    var now = new Date()
    var thisYear = now.getFullYear()
    if (arr.length != 0) {
    for (var i = 0; i < arr.length; i++) {
        var ageNow = thisYear - arr[i][3];
        if (ageNow > 0 && ageNow < 100) {
            d = {firstName: arr[i][0], lastName: arr[i][1], gender: arr[i][2], age: ageNow};
           
        } else {
            d = {firstName: arr[i][0], lastName: arr[i][1], gender: arr[i][2], age: "Invalid Birth Year"};
        }
        console.log(i+1 + ". " + arr[i][0] + " " + arr[i][1] + ": ", d)
    }

     }else {
    console.log('" "');
  }
}
console.log("-----------------soal 1 --------------------")
people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people)
arrayToObject(people2)
arrayToObject([])

// Soal 2

function shoppingTime(memberId, money) {
    var object = {
        'Sepatu Stacattu': 1500000,
        'Baju Zoro': 500000,
        'Baju H&N': 250000,
        'Sweater Uniklooh': 175000,
        'Casing Handphone': 50000
    };
    var listPurchased = [];
    if (!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup';
    } else {
        var counter = money;
        for (const key of Object.keys(object)) {
            if (counter >= object[key]) {
                listPurchased.push(key);
                counter -= object[key];
            }
        }
        return { memberId: memberId, money: money, listPurchased: listPurchased, changeMoney: counter };
    }
}

console.log("-----------------soal 2 --------------------")
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000));
  console.log(shoppingTime('234JdhweRxa53', 15000));
  console.log(shoppingTime());

// Soal 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var d = {};
    
    for (var i = 0; i < arrPenumpang.length; i++) {
        var asal = arrPenumpang[i][1];
        var tiba = arrPenumpang[i][2];
        var jarak = 0;
        for (var k=0; k<rute.length; k++) {
            if (rute[k] === asal) {
                for (var l=k+1; l<rute.length; l++) {
                    jarak += 1;
                    if (rute[l] === tiba) {
                        var totalBayar = jarak * 2000;
                    }
                }
            }
        }
        d = {penumpang: arrPenumpang[i][0], naikDari: asal, tujuan: tiba, bayar: totalBayar};
        console.log(d)
    }
    return "";
  }
  console.log("-----------------soal 3 --------------------")
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  console.log(naikAngkot([])); 
  console.log("----------------- Selesai --------------------")
