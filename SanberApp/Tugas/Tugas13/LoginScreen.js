import React from 'react'
import {View, Image, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LoginScreen extends React.Component{
    constructor() {
        super();
        this.state = {
            username: 'Username',
            password: 'Password',
        };
    }
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.topNavBar}>
                    <Text style={{fontSize: 15, color: '#ffffff', fontFamily:'sans-serif', fontWeight:'bold'}}>Sanber App</Text>
                </View>
                <View style={styles.body}>
                    <Image source={require('./images/logo.png')} style={styles.largeLogo}/>
                    <Text style={styles.greetingText}>Login</Text>
                    <View style={styles.loginForm}>
                        <View style={styles.textFieldContainer}>
                            <Icon name='mail-outline' size={25}/>
                            <TextInput
                            style={styles.textInput}
                            onChangeText={(text) => this.setState({ username: text })}
                            value={this.state.username}
                            />
                        </View>
                        <View style={{marginBottom:10, height:0.5, backgroundColor:'#E5E5E5'}}/>
                        
                        <View style={styles.textFieldContainer}>
                            <Icon name='lock' size={25}/>
                            <TextInput
                            style={styles.textInput}
                            onChangeText={(text) => this.setState({ password: text })}
                            value={this.state.password}
                            />
                        </View>
                        <View style={{height:0.5, backgroundColor:'#E5E5E5'}}/>
                    </View>
                    <TouchableOpacity style={styles.loginButton}>
                        <Text style={{fontSize: 15, color: 'white', fontWeight: 'bold'}}>Masuk</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.signButton}>
                        <Text style={{fontSize: 15, color: 'white', fontWeight: 'bold'}}>Daftar</Text>
                    </TouchableOpacity>
                    <View style={styles.signUpContainer}>
                        <Text style={{fontSize: 13, color: '#C0C0C0'}}>Don't have an account? </Text>
                        <Text style={{fontSize: 13 }}>Sign-up Here</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    topNavBar:{
        height: 80,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#56B8FF',
        paddingTop: 45,
        elevation: 3
    },
    body:{
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
    },
    largeLogo:{
        height: 100,
        width: 300,
        justifyContent: "center",
        marginTop: 50
    },
    greetingText:{
        paddingTop: 55,
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 35
    },
    loginForm:{
        flexDirection: "column",
        justifyContent: "center",
    },
    textFieldContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10
    },
    textInput:{
        height: 40,
        width: 250,
        paddingLeft: 10
    },
    loginButton:{
        backgroundColor: '#56B8FF',
        paddingHorizontal: 50,
        paddingVertical: 10,
        borderRadius: 25,
        marginVertical: 25
    },
    signButton:{
        backgroundColor: '#003366',
        paddingHorizontal: 50,
        paddingVertical: 10,
        borderRadius: 25,
        marginVertical: 25
    },
    signUpContainer:{
        flex: 1,
        flexDirection: "row"
    }
})