import React from 'react'
import {View, Image, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AntDesign } from '@expo/vector-icons';

export default class LoginScreen extends React.Component{
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.topNavBar}>
                    <Text style={{fontSize: 15, color: '#56B8FF', fontFamily:'sans-serif', fontWeight:'bold'}}>Sanbercode.id</Text>
                </View>
                <Text style={styles.title}>Home.</Text>
                <View style={{alignItems: "center", padding:15}}>
                    <Image source={{uri:'https://www.pngitem.com/pimgs/m/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png'}} style={styles.devImage}/>
                </View>
                <Text style={{fontSize:20, textAlign: "center", fontWeight: "bold"}}>CodeAflaha</Text>
                <Text style={{fontSize:18, textAlign: "center", fontWeight: "bold", color: '#56B8FF'}}>React Native Developer</Text>
                
                <View style={styles.sosmedCard}>
                    <Text style={styles.cardTitle}>Portofolio</Text>
                    <View style={{height:0.5, backgroundColor:'#56B8FF'}}/>
                    <View style={styles.portofolioContainer}>
                        <View style={styles.account_vert}>
                            <AntDesign style={{margin:5}} name="gitlab" size={40} color="#56B8FF" />
                            <Text>@CodeAflaha</Text>
                        </View>
                        <View style={styles.account_vert}>
                            <AntDesign style={{margin:5}} name="github" size={40} color="#56B8FF" />
                            <Text>@CodeAflaha</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.sosmedCard}>
                    <Text style={styles.cardTitle}>Hubungi Saya</Text>
                    <View style={{height:0.5, backgroundColor:'#56B8FF'}}/>
                    <View style={styles.kontakContainer}>
                        <View style={styles.account_horizontal}>
                            <AntDesign style={{margin:5}} name="facebook-square" size={40} color="#56B8FF" />
                            <Text>CodeAflaha</Text>
                        </View>
                        <View style={styles.account_horizontal}>
                            <AntDesign style={{margin:5}} name="instagram" size={40} color="#56B8FF" />
                            <Text>@CodeAflaha</Text>
                        </View>
                        <View style={styles.account_horizontal}>
                            <AntDesign style={{margin:5}} name="twitter" size={40} color="#56B8FF" />
                            <Text>@CodeAflaha</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    topNavBar: {
        height: 80,
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 45,
        elevation: 3
    },
    title:{
        textAlign: "center",
        fontSize: 28,
        fontWeight: "bold"
    },
    devImage:{
        height: 200,
        width: 200,
        borderRadius: 250,
    },
    sosmedCard:{
        backgroundColor: '#EFEFEF',
        borderRadius: 20,
        padding: 15,
        margin: 15
    },
    cardTitle:{
        fontSize: 15,
        fontFamily: 'Roboto',
        color: '#003366'
    },
    portofolioContainer:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignContent: "center"
    },
    kontakContainer:{
        justifyContent: 'center',
        paddingLeft: 70
    },
    account_vert:{
        flexDirection: "column",
        alignItems: "center",
    },
    account_horizontal:{
        flexDirection: "row",
        alignItems: "center",
        margin: 5
    }
})