import React from 'react'
import {View,StyleSheet,Text,StatusBar,Image} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class SkillItem extends React.Component{
    render(){
        let content = this.props.content
        return(
            <View style={styles.container}>
                <View style={styles.box}>
                    <Icon name={content.iconName} size={90} color={'#003366'} style={{marginLeft:10}}/>
                    <View style={{marginLeft:25,flexDirection:'column',alignSelf:'flex-start',marginTop:10}}>
                        <View style={{height:108,width:165,backgroundColor:'#b4e9ff'}}>
                            <Text style={{fontSize:24,color:'#003366',fontWeight:'bold'}}>{content.skillName}</Text>
                            <Text style={{fontSize:16,color:'#3ec6ff',fontWeight:'bold'}}>{content.categoryName}</Text>
                            <Text style={{fontSize:48,color:'white',fontWeight:'bold',alignSelf:'flex-end'}}>{content.percentageProgress}</Text>
                        </View>
                    </View>
                    <Icon name='chevron-right' size={60} color={'#003366'}/>
                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        padding:4
    },
    box:{
        height: 129,
        width:343,
        backgroundColor:'#B4e9ff',
        borderRadius:8,
        flexDirection:'row',
        marginLeft:10,
        elevation:4,
        alignItems:'center'
    }
})