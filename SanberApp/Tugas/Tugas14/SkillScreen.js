import React from 'react'
import {View,StyleSheet,Text,StatusBar,Image, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import SkillItem from './SkillItem'
import data from './skillData.json'

export default class SkillScreen extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <StatusBar/>
                <Image source={require('../Tugas13/images/logo.png')} style={{height:51,width:187.5,alignSelf:'flex-end'}}/>
                <View style={styles.account}>
                    <Icon name='account-circle' color='#3ec6ff' size={35} style={{marginTop:4}}/>
                    <View style={{marginLeft:11,flexDirection:'column'}}>
                        <Text style={{fontSize:12}}>Hai,</Text>
                        <Text style={{fontSize:16,color:'#003366'}}>Code Aflaha</Text>
                    </View>
                </View>
                <View style={{marginTop:16,marginLeft:16,marginRight:16}}>
                    <Text style={{fontSize:36,color:'#003366'}}>SKILL</Text>
                    <View style={{height:4,backgroundColor:'#3ec6ff'}}/>
                </View>
                <View style={styles.category}>
                    <View style={{height:32,width:125,justifyContent:'center',alignItems:'center',borderRadius:8,backgroundColor:'#b4e9ff'}}>
                        <Text style={{fontSize:12,fontWeight:'bold',color:'#003366'}}>Library / Framework</Text>
                    </View>
                    <View style={{height:32,width:136,marginLeft:6,justifyContent:'center',alignItems:'center',borderRadius:8,backgroundColor:'#b4e9ff'}}>
                        <Text style={{fontSize:12,fontWeight:'bold',color:'#003366'}}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={{height:32,width:70,marginLeft:6,justifyContent:'center',alignItems:'center',borderRadius:8,backgroundColor:'#b4e9ff'}}>
                        <Text style={{fontSize:12,fontWeight:'bold',color:'#003366'}}>Teknologi</Text>
                    </View>
                </View>
                <View style={{paddingBottom:6}}/>
                <FlatList
                data={data.items}
                renderItem={(content)=><SkillItem content={content.item}/>}
                keyExtractor={(item)=>item.id}
                />
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1
    },
    account:{
        marginTop:3,
        marginLeft:19,
        flexDirection:'row'        
    },
    category:{
        flexDirection:'row',
        marginTop:10,
        marginLeft:16,
        marginRight:16
    },
    skillList:{
        flex:1,
    }
}

)
