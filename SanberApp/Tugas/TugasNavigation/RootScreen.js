import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
);
  
export const Root = ({ navigation }) => (
    <ScreenContainer>
        <Text>Root Navigation Screen</Text>
        <Button title="Login" onPress={() => navigation.push('Login')} />
        <Button title="Drawer" onPress={() => navigation.push('Drawer')} />
    </ScreenContainer>
);