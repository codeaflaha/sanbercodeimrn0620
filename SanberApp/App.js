import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';
//import YoutubeUI from './Tugas/Tugas12/App';
//import Tugas13 from './Tugas/Tugas13/LoginScreen'
//import AboutScreen from './Tugas/Tugas13/AboutScreen'
//import App14 from './Tugas/Tugas14/App'
//import SkillScreen from './Tugas/Tugas14/SkillScreen'
//import Tugas15_1 from './Tugas/Tugas15/index';
//import TugasNavigation from './Tugas/TugasNavigation/index';
import Main from "./Tugas/Quiz3/index.js";

export default function App() {
  return (
    <Main/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
