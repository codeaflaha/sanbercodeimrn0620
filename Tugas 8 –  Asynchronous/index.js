console.log('No. 1 (Callback Baca Buku)')
console.log('======================')

var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function bookRead(i,times){
    if(i > books.length-1){
        return ;

    }
    else{
        readBooks(times,books [i],function (times) {
            return i + bookRead (i + 1,times)

        })
    }
}

bookRead(0,10000)
