// soal 1

class Animal{
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}
console.log("---------------soal 1 Release 0 ------------------")
var sheep = new Animal("shaun")
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

class Ape extends Animal {
    constructor(name){
        super(name)
        this.legs = 2
    }
    yell() {
            console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name){
        super(name)
        this.cold_blooded = true
    }
    jump(){
        console.log("hop hop")
        //return "hop hop"
    }
}
console.log("---------------soal 1 Release 1 ------------------")
var sungokong = new Ape("kera sakti")
sungokong.yell()
var kodok = new Frog("buduk")
kodok.jump()



// soal 2
class Clock {
    
    constructor({template}){
        this.template = template;
        this.timer
    }
    
    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        
        console.log(output);
    };

    stop = function() {
        clearInterval(this.timer);
    };

    start = function() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    };
}
console.log("---------------soal 2 ------------------")
var clock = new Clock({template: 'h:m:s'});
clock.start();

